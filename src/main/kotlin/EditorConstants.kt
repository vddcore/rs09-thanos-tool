import com.github.weisj.darklaf.LafManager
import com.github.weisj.darklaf.theme.OneDarkTheme

object EditorConstants {
    var BUILD_NUMBER = "1.3.0"

    var DARK_MODE = true

    var CONFIG_PATH = ""

    var VALID_FILES = arrayOf("drop_tables.json","npc_configs.json","item_configs.json", "shops.json", "object_configs.json")

    var CREDITS = arrayOf(
        "weisJ - darklaf look and feel themes http://github.com/weisj",
        "ceikry - design and programming http://gitlab.com/ceikry"
    )

    fun updateTheme(){
        if(EditorConstants.DARK_MODE) LafManager.install(OneDarkTheme()) else LafManager.install()
    }
}